package com.wk.twitter.twitterapp.service;

import com.wk.twitter.twitterapp.model.dto.CommentDto;
import com.wk.twitter.twitterapp.model.dto.MessageDto;
import com.wk.twitter.twitterapp.model.entity.Message;
import com.wk.twitter.twitterapp.model.entity.User;
import com.wk.twitter.twitterapp.model.entity.UserDetails;
import com.wk.twitter.twitterapp.repository.MessageRepository;
import com.wk.twitter.twitterapp.repository.UserDetailsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserDetailsRepository  userDetailsRepository;

    public List<Message> getAllMessages() {
        return messageRepository.findAllByOrderByIdDesc();
    }

    public void addMessage(MessageDto messageDto) {
        String username = getCurrentUserFromContext();
        Message mappedMessage = mapper.map(messageDto, Message.class);
        User currentLogInUser = userDetailsRepository.findUserByLogin(username);
        mappedMessage.setUser(currentLogInUser);
        mappedMessage.setLikes(0L);
        mappedMessage.setParentId(null);
        messageRepository.save(mappedMessage);
    }

    public void addComment(CommentDto commentDto) {
        String username = getCurrentUserFromContext();
        User currentLogInUser = userDetailsRepository.findUserByLogin(username);
        Message mes = new Message(commentDto.getMessage(), 0L, commentDto.getParentId(), currentLogInUser);
        messageRepository.save(mes);
    }

    @Transactional
    public void deleteMessage(MessageDto messageDto) {
        Optional<Message> msg = messageRepository.findById(messageDto.getId());
        if(msg.isPresent()) {
            if (getCurrentUserFromContext().equals(((UserDetails)msg.get().getUser()).getLogin())) {
                messageRepository.deleteById(messageDto.getId());
            } else {
                throw new RuntimeException("Not your message!");
            }
        }
    }

    public UserDetails getCurrentUser() {
        return userDetailsRepository.findUserByLogin(getCurrentUserFromContext());
    }

    private String getCurrentUserFromContext() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof org.springframework.security.core.userdetails.UserDetails) {
            username = ((org.springframework.security.core.userdetails.UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }
}
