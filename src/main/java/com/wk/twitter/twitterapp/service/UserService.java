package com.wk.twitter.twitterapp.service;

import com.wk.twitter.twitterapp.model.dto.RegisterUserDto;
import com.wk.twitter.twitterapp.model.entity.User;
import com.wk.twitter.twitterapp.model.entity.UserDetails;
import com.wk.twitter.twitterapp.repository.UserDetailsRepository;
import com.wk.twitter.twitterapp.repository.UserRepository;
import com.wk.twitter.twitterapp.service.validator.BindingValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private ModelMapper mapper;

    public void registerUser(RegisterUserDto user, BindingResult result) {
        BindingValidator.validate(result);
        String hash = bCryptPasswordEncoder.encode(user.getPassword());
        UserDetails newUser = setNewUser(user, hash);
        userDetailsRepository.save(newUser);
    }

    private UserDetails setNewUser(RegisterUserDto user, String hash) {
        UserDetails newUser = mapper.map(user, UserDetails.class);
        newUser.setPassword(hash);
        newUser.setRole("USER");
        return newUser;
    }
}
