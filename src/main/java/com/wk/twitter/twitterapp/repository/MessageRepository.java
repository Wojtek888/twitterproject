package com.wk.twitter.twitterapp.repository;

import com.wk.twitter.twitterapp.model.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findAllByOrderByIdDesc();
}
