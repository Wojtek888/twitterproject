package com.wk.twitter.twitterapp.repository;

import com.wk.twitter.twitterapp.model.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
    UserDetails findUserByLogin(String login);
}
