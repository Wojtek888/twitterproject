package com.wk.twitter.twitterapp.repository;

import com.wk.twitter.twitterapp.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
}
