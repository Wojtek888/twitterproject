package com.wk.twitter.twitterapp.controller;

import com.wk.twitter.twitterapp.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = {"/","/index"})
    public ModelAndView usersView(Model model) {
        Map<String, Object> buildModel = new HashMap<String, Object>();
        buildModel.put("allMessages", messageService.getAllMessages());
        buildModel.put("currentUser", messageService.getCurrentUser());
        return new ModelAndView("index","model", buildModel);
    }
}
