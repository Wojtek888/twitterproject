package com.wk.twitter.twitterapp.controller;

import com.wk.twitter.twitterapp.model.dto.MessageDto;
import com.wk.twitter.twitterapp.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MessageController {

    @Autowired
    MessageService messageService;

    @PostMapping("/addpost")
    public String addNewPost(@ModelAttribute("newmessage") MessageDto messageDto) {
        messageService.addMessage(messageDto);
        return "messagesavedsuccess";
    }

    @PostMapping("/deletepost")
    public String deletePost(@ModelAttribute("message") MessageDto messageDto) {
        messageService.deleteMessage(messageDto);
        return "messagedeletesuccess";
    }
}
