package com.wk.twitter.twitterapp.controller;

import com.wk.twitter.twitterapp.model.dto.RegisterUserDto;
import com.wk.twitter.twitterapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegisterUserController {

    @Autowired
    UserService userService;

    @GetMapping("/registeruser")
    public ModelAndView createNewUser() {
        return new ModelAndView("registeruserform","userToInsert", new RegisterUserDto());
    }

    @PostMapping("/registeruser")
    public String addNewUser(@ModelAttribute("userToInsert") @Valid RegisterUserDto user, BindingResult result) {
        if(result.hasErrors()) {
            return "registeruserform";
        }
        userService.registerUser(user, result);
        return "userregistersuccess";
    }
}
