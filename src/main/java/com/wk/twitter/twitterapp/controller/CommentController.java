package com.wk.twitter.twitterapp.controller;

import com.wk.twitter.twitterapp.model.dto.CommentDto;
import com.wk.twitter.twitterapp.model.dto.MessageDto;
import com.wk.twitter.twitterapp.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CommentController {

    @Autowired
    MessageService messageService;

    @PostMapping("/addcomment")
    public String addNewComment(@ModelAttribute("newcomment") CommentDto commentDto) {
        messageService.addComment(commentDto);
        return "commentsavedsuccess";
    }

    @PostMapping("/deletecomment")
    public String deleteComment(@ModelAttribute("comment") MessageDto messageDto) {
        messageService.deleteMessage(messageDto);
        return "messagedeletesuccess";
    }
}
