package com.wk.twitter.twitterapp.model.dto;

public class MessageDto {

    private Long id;
    private String message;

    public MessageDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
