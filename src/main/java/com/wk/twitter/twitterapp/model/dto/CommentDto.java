package com.wk.twitter.twitterapp.model.dto;

public class CommentDto {

    private Long id;
    private String message;
    private Long parentId;

    public CommentDto() {
    }
    public CommentDto(Long parentId) {
        this.parentId=parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
