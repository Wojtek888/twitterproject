package com.wk.twitter.twitterapp.model.dto;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Size;

public class RegisterUserDto {

    @Size(min = 3, max = 20, message = "Name length should be between 3 and 20 characters.")
    private String name;
    @Size(min = 3, max = 20, message = "Surname length should be between 3 and 20 characters.")
    private String surname;
    @URL(message = "Img should be URL link")
    private String userImg;
    @Size(min = 3, max = 10, message = "Login length should be between 3 and 10 characters.")
    private String login;
    @Size(min = 3, max = 10, message = "Password length should be between 3 and 10 characters.")
    private String password;

    public RegisterUserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
