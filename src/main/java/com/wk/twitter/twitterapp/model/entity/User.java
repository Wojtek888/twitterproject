package com.wk.twitter.twitterapp.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    @Column(name="user_img")
    private String userImg;
//    @OneToOne
//    @JoinColumn(name = "id_details")
//    private UserDetails details;
    @OneToMany(mappedBy = "user")
    private List<Message> messages;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

//    public UserDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(UserDetails details) {
//        this.details = details;
//    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
